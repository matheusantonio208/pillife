'use strict';

const mongoose = require('mongoose');
const Schema = new mongoose.Schema({
    id_belongs_to:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'finance_wallet finance_card finance_bank',
        require: true
    },
    transaction_name: {
        type: String,
        required: true
    },
    transaction_description: {
        type: String
    },
    transaction_status: {
        type: String,
        enum: ['paid','planned','received'],
        required: true
    },
})

module.exports = mongoose.model('finance-transaction', Schema);