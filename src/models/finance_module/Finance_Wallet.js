'use strict';

const mongoose = require('mongoose');
const Schema = new mongoose.Schema({
    id_owner_user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user-module',
        required: true
    },
    wallet_name:{
        type: String,
        required: true
    },
    wallet_initial_value:{
        type: Number
    },
    wallet_coin:{
        type: String,
        enum: ['BRL','EUR','USA'],
        required: true
    }
});

module.exports = mongoose.model('finance-wallet',Schema);