'use strict';

const mongoose = require('mongoose');
const schema = new mongoose.Schema({

    user_email: {
        type: String,
        required: true,
        unique: true
    },

    user_password: {
        type: String,
        required: true
    },

    user_full_name: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('user-objects', schema);