'use strict'

const express = require('express');
const router = express.Router();

router.get('/', async (req, res, next) => {
    res.render('pages/generic_pages/index.hbs');
});

router.get('/404', async (req, res, next) => {
    res.render('pages/generic_pages/404.hbs');
});

router.get('/500', async (req, res, next) => {
    res.render('pages/generic_pages/500.hbs');
});

router.get('/maintence', async (req, res, next) => {
    res.render('pages/generic_pages/maintence.hbs');
});

module.exports = router;