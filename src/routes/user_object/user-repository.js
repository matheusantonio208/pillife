'use strict';

const mongoose = require('mongoose');
const User = mongoose.model('user-objects');
const passport = require('passport');

exports.getUserById = async (userId) => {
    var user = User.findById(userId);
    return user;
}

exports.setUser = async (userDataToSet) => {
    var user = new User(userDataToSet);
    await user.save();
}

exports.updateUserById = async (userId, userDataToUpdate) => {
    await User.findByIdAndUpdate(userId, userDataToUpdate);
}

exports.deleteUserById = async (userId) => {
    await User.findOneAndDelete(userId);
}
