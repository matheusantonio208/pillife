'use strict'

const express = require('express');
const router = express.Router();
const User = require('./user-controller');

router.get('/register', async (req, res, next) => {
    res.render('pages/user_behaviours/user-register.hbs');
});
router.post('/register', User.setUser);

router.get('/confirm-email', async (req, res, next) => {
    res.render('pages/user_behaviours/user-confirm-email.hbs');
});

router.get('/login', async (req, res, next) => {
    res.render('pages/user_behaviours/user-login.hbs');
});
router.post('/login', User.authenticateUser);

router.get('/dashboard', User.getDashboardPage);

router.get('/:userId', User.getUserById);
router.put('/:userId', User.updateUserById);
router.delete('/:userId', User.deleteUserById);

module.exports = router;