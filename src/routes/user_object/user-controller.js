'use strict';

const User = require('./user-repository');

const bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);
const passport = require('passport');

exports.getUserById = async (req, res, next) => {
    try {
        var user = await User.getUserById(req.params.userId);
        res.status(200).send(user);

    } catch (error) {
        res.status(500).send('Erro ao capturar usuário: ' + error)
    }
}

exports.setUser = async (req, res, next) => {
    try {
        await User.setUser({
            user_email: req.body.emailaddress,
            user_password: bcrypt.hashSync(req.body.password, salt),
            user_full_name: req.body.fullname
        });
        res.status(201).redirect('/confirm-email');

    } catch (error) {
        res.status(500).send('Erro ao criar usuário: ' + error)
    }
}

exports.updateUserById = async (req, res, next) => {
    try {
        await User.updateUserById(req.params.userId, req.body);
        res.status(200).send('Usuário atualizado com sucesso!')
    } catch (error) {
        req.status(500).send('Erro ao atualizar usuário: ' + error)
    }
}

exports.deleteUserById = async (req, res, next) => {
    try {
        await User.deleteUserById(req.params.userId);
        res.status(410).send('Usuário deletado com sucesso!')

    } catch (error) {
        req.status(500).send('Erro ao deletar usuário: ' + error)
    }
}

exports.authenticateUser = async (req, res, next) => {
    try {
        passport.authenticate('local',{
            successRedirect: '/dashboard',
            failureRedirect: '/login'
        })(req, res, next)
        
    } catch (error) {
        res.status(500).send("Erro ao efetuar login!")
    }
}


exports.getDashboardPage = async (req, res, next) => {
    try{
        res.render('pages/user_behaviours/user-dashboard.hbs', {layout: 'dashboard.hbs'})
    } catch (error) {

    }
}
