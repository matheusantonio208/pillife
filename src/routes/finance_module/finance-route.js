'use strict'

const express = require('express');
const router = express.Router();
const Finance = require('./finance-controller');
/* WALLETS */
router.get('/:userId/wallet', Finance.getAllWalletsByUserId);
router.get('/:userId/wallet/:walletId', Finance.getOneWalletByUserId);
router.post('/wallet/', Finance.setWallet);
router.put('/wallet/:walletId', Finance.updateWalletById);
router.delete('/wallet/:walletId', Finance.deleteWalletById);

/* TRANSACTIONS */
router.get('/:walletId/transaction', Finance.getAllTransactionsByBelongingId);
router.get('/:walletId/transaction/:transactionId', Finance.getOneTransactionByBelongingId);
router.post('/transaction/', Finance.setTransaction);
router.put('/transaction/:transactionId', Finance.updateTransactionById);
router.delete('/transaction/:transactionId', Finance.deleteTransactionById);

module.exports = router;