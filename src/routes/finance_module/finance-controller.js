'use strict';

const Finance = require('./finance-repository');

/* WALLETS */
exports.getAllWalletsByUserId = async (req, res, next) => {
    try {
        var wallets = await Finance.getAllWalletsByUserId(req.params.userId);
        res.status(200).send(wallets);

    } catch (error) {
        res.status(500).send('Erro ao listar todas as carteiras: ' + error)
    }
}
exports.getOneWalletByUserId = async (req, res, next) => {
    try {
        var wallet = await Finance.getOneWalletByUserId(req.params.userId, req.params.walletId);
        res.status(200).send(wallet);

    } catch (error) {
        res.status(500).send('Erro ao listar a carteira do usuário: ' + error)
    }
}
exports.setWallet = async (req, res, next) => {
    try {
        await Finance.setWallet(req.body);
        res.status(201).send('Carteira criada com sucesso');

    } catch (error) {
        res.status(500).send('Erro ao criar carteira: ' + error)
    }
}
exports.updateWalletById = async (req, res, next) => {
    try {
        await Finance.updateWalletById(req.params.walletId, req.body);
        res.status(200).send('Carteira atualizada com sucesso');

    } catch (error) {
        res.status(500).send('Erro ao atualizar carteira: ' + error)
    }
}
exports.deleteWalletById = async (req, res, next) => {
    try {
        await Finance.deleteWalletById(req.params.walletId);
        res.status(410).send('Carteira deletada com sucesso');

    } catch (error) {
        res.status(500).send('Erro ao deletar a carteira: ' + error)
    }
}

/* TRANSACTIONS */
exports.getAllTransactionsByBelongingId = async (req, res, next) => {
    try {
        var wallets = await Finance.getAllTransactionsByBelongingId(req.params.walletId);
        res.status(200).send(wallets);

    } catch (error) {
        res.status(500).send('Erro ao listar todas as transação: ' + error)
    }
}
exports.getOneTransactionByBelongingId = async (req, res, next) => {
    try {
        var wallet = await Finance.getOneTransactionByBelongingId(req.params.walletId, req.params.transactionId);
        res.status(200).send(wallet);

    } catch (error) {
        res.status(500).send('Erro ao listar a transação do usuário: ' + error)
    }
}
exports.setTransaction = async (req, res, next) => {
    try {
        await Finance.setTransaction(req.body);
        res.status(201).send('Carteira criada com sucesso');

    } catch (error) {
        res.status(500).send('Erro ao criar carteira: ' + error)
    }
}
exports.updateTransactionById = async (req, res, next) => {
    try {
        await Finance.updateTransactionById(req.params.transactionId, req.body);
        res.status(200).send('Transação atualizada com sucesso!');

    } catch (error) {
        res.status(500).send('Erro ao atualizar transação: ' + error)
    }
}
exports.deleteTransactionById = async (req, res, next) => {
    try {
        await Finance.deleteTransactionById(req.params.transactionId);
        res.status(410).send('Transação deletada com sucesso!');

    } catch (error) {
        res.status(500).send('Erro ao deletar a transação: ' + error)
    }
}


















/*
= async (req, res, next) => {
    try {

        res.status(201).send('__ com sucesso');

    } catch (error) {
        res.status(500).send('Erro ao __: ' + error)
    }
}
*/