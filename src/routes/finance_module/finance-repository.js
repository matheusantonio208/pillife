'use strict';

const mongoose = require('mongoose');
const FinanceWallet = mongoose.model('finance-wallet');
const FinanceTransaction = mongoose.model('finance-transaction');

/* WALLETS */
exports.getAllWalletsByUserId = async (userId) => {
    var wallets = await FinanceWallet.find({'id_owner_user':userId});
    return wallets;
}
exports.getOneWalletByUserId = async(userId, walletId) => {
    var wallet = await FinanceWallet.findOne({'id_owner_user':userId, '_id':walletId});
    return wallet;
}
exports.setWallet = async (walletDataToSet) => {
    var wallet = new FinanceWallet(walletDataToSet);
    await wallet.save();
}
exports.updateWalletById = async (walletId, walletDataToUpdate) => {
    await FinanceWallet.findByIdAndUpdate(walletId, walletDataToUpdate);
}
exports.deleteWalletById = async (walletId) => {
    await FinanceWallet.findByIdAndDelete(walletId);
}

/* TRANSACTIONS */
exports.getAllTransactionsByBelongingId = async (walletId) => {
    var transactions = await FinanceTransaction.find({'id_belongs_to':walletId});
    return transactions;
}
exports.getOneTransactionByBelongingId = async(walletId, transactionId) => {
    var transaction = await FinanceTransaction.findOne({'id_belongs_to':walletId, '_id':transactionId});
    return transaction;
}
exports.setTransaction = async (transactionDataToSet) => {
    var transaction = new FinanceTransaction(transactionDataToSet);
    await transaction.save();
}
exports.updateTransactionById = async (transactionId, transactionDataToUpdate) => {
    await FinanceTransaction.findByIdAndUpdate(transactionId, transactionDataToUpdate);
}
exports.deleteTransactionById = async (transactionId) => {
    await FinanceTransaction.findByIdAndDelete(transactionId);
}