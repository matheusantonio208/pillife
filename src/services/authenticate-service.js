const mongoose = require('mongoose');
const userSchema = require('../models/user_object/User.js')
const User = mongoose.model('user-objects');

const userRepository = require('../routes/user_object/user-repository.js')

const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

module.exports = function(passport){
    passport.use(new LocalStrategy({usernameField: 'user_email', passwordField:'user_password'}, (userEmail, password, done) => {
        User.findOne({user_email: userEmail},(error, user)=>{
            if(!user)
                return done(null, false, {message: 'Este usuário não está cadastrado!'}); 
    
            var isPassword = bcrypt.compareSync(password, user.user_password); 
            if(!isPassword)
                return done(null, false, {message: 'A senha está incorreta!'});
            
            return done(null, user);
        });
    }));

    passport.serializeUser((user, done) => {
        done(null, user._id)
    });

    passport.deserializeUser((id, done) => {
        var userById = userRepository.getUserById(id);
        if(userById)
            done(null, userById);
    });
}