'use strict';

const express = require('express');
const app = express();
const config = require('./config-vars');

//SECTION
const session = require('express-session');
const passport = require('passport');
const authService = require('../src/services/authenticate-service.js')(passport);

app.use(session({
    secret: 'user_section',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

//STATIC DIRECTORIES
const path = require('path');
app.use('/modules', express.static(path.join(__dirname, '../node_modules')));
app.use('/views', express.static(path.join(__dirname, '../src/views')));
app.use('/assets', express.static(path.join(__dirname, '../src/assets')));

//HANDLEBARS
const handlebars = require('express-handlebars').create({
	defaultLayout: 'main',
	extname: '.hbs',
	helpers: path.join(__dirname, '../src/views/helpers'),
	layoutsDir: path.join(__dirname, '../src/views/layouts'),
	partialsDir: path.join(__dirname, '../src/views/partials'),
	
});
app.engine('.hbs', handlebars.engine);
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, "../src/views"));

//BODY PARSER
const bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({extended: false}));

//CORS
app.use(function (req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Orign, X-Requested-With, Content-Type, Accept, x-access-token');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
})

//DATABASE
const mongoose = require('mongoose');
mongoose.connect(config.databaseConnectionPath, {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, dbName:"pillife-main"});
mongoose.set('useFindAndModify', false);


//MODELS
const userModule = require('../src/models/user_object/User.js');
const financeWallet = require('../src/models/finance_module/Finance_Wallet.js');
const financeTransaction = require('../src/models/finance_module/Finance_Transaction.js');

//ROUTERS
const genericRoutes = require('../src/routes/generic_routes/generic-routes.js');
const userRoute = require('../src/routes/user_object/user-route.js');
const financeRoute = require('../src/routes/finance_module/finance-route.js');
app.use('/', genericRoutes);
app.use('/', userRoute);
app.use('/finance', financeRoute);

module.exports = app;